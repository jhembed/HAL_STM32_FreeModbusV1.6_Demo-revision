# HAL_STM32_FreeModbusV1.6_Demo

#### 介绍
STM32基于HAL库的移植FreeModbus示例工程

移植教程见博客地址：https://blog.csdn.net/qq153471503/article/details/104840279

qmodbus上位机软件下载地址：https://gitee.com/yzhengBTT/qmodbus/releases/V1.0

#### 说明
此版本为修改版，当主机读取线圈或者寄存器的数量大于从机实际存在的数量时，从机按照实际能够读取到的线圈或寄存器进行返回。

例如，当从机只有10路保持寄存器，而主机读取20路保持寄存器，那么从机将返回10路寄存器的数据。

