#include "mb.h"
#include "mbport.h"


// 十路输入寄存器
#define REG_INPUT_SIZE  10
uint16_t REG_INPUT_BUF[REG_INPUT_SIZE];


// 十路保持寄存器
#define REG_HOLD_SIZE   10
uint16_t REG_HOLD_BUF[REG_HOLD_SIZE];


// 十路线圈
#define REG_COILS_SIZE 10
uint8_t REG_COILS_BUF[REG_COILS_SIZE] = {1, 1, 1, 1, 0, 0, 0, 0, 1, 1};


// 十路离散量
#define REG_DISC_SIZE  10
uint8_t REG_DISC_BUF[REG_DISC_SIZE] = {1,1,1,1,0,0,0,0,1,1};


eMBErrorCode eMBRegInputCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, USHORT* pusRegCountResponse)
{
    USHORT usRegIndex = usAddress - 1;

    // 非法检测
    if(usRegIndex >= REG_INPUT_SIZE)
    {
        return MB_ENOREG;
    }

    *pusRegCountResponse = 0;  // 实际读取到的寄存器数量

    // 循环读取
    while( usNRegs > 0 && usRegIndex < REG_INPUT_SIZE)
    {
        *pucRegBuffer++ = ( unsigned char )( REG_INPUT_BUF[usRegIndex] >> 8 );
        *pucRegBuffer++ = ( unsigned char )( REG_INPUT_BUF[usRegIndex] & 0xFF );
        usRegIndex++;
        usNRegs--;
        (*pusRegCountResponse)++;
    }

    // 模拟输入寄存器被改变
    for(usRegIndex = 0; usRegIndex < REG_INPUT_SIZE; usRegIndex++)
    {
        REG_INPUT_BUF[usRegIndex]++;
    }

    return MB_ENOERR;
}


/// CMD6、3、16命令处理回调函数
eMBErrorCode eMBRegHoldingCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNRegs, USHORT* pusRegCountResponse, eMBRegisterMode eMode)
{
    USHORT usRegIndex = usAddress - 1;

	*pusRegCountResponse = 0;

    if(eMode == MB_REG_WRITE)
    {
        // 非法检测
        if((usRegIndex + usNRegs) > REG_HOLD_SIZE)
        {
            return MB_ENOREG;
        }
		
        while( usNRegs > 0 )
        {
            REG_HOLD_BUF[usRegIndex] = (pucRegBuffer[0] << 8) | pucRegBuffer[1];
            pucRegBuffer += 2;
            usRegIndex++;
            usNRegs--;
			(*pusRegCountResponse)++;
        }
    }
    else
    {
        // 非法检测
        if(usRegIndex >= REG_HOLD_SIZE)
        {
            return MB_ENOREG;
        }
		
        while( usNRegs > 0 && usRegIndex < REG_HOLD_SIZE)
        {
            *pucRegBuffer++ = ( unsigned char )( REG_HOLD_BUF[usRegIndex] >> 8 );
            *pucRegBuffer++ = ( unsigned char )( REG_HOLD_BUF[usRegIndex] & 0xFF );
            usRegIndex++;
            usNRegs--;
			(*pusRegCountResponse)++;
        }
    }

    return MB_ENOERR;
}

/// CMD1、5、15命令处理回调函数
eMBErrorCode eMBRegCoilsCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNCoils, USHORT* pusNCoilsResponse, eMBRegisterMode eMode )
{
    USHORT usRegIndex    = usAddress - 1;
	USHORT usReadableCnt = 0;
    UCHAR  ucBits        = 0;
    UCHAR  ucState       = 0;
    UCHAR  ucLoops       = 0;

    *pusNCoilsResponse = 0;

    if(eMode == MB_REG_WRITE)
    {
        // 非法检测
        if((usRegIndex + usNCoils) > REG_COILS_SIZE)
        {
            return MB_ENOREG;
        }

        ucLoops = (usNCoils - 1) / 8 + 1;
        while(ucLoops != 0)
        {
            ucState = *pucRegBuffer++;
            ucBits  = 0;
            while(usNCoils != 0 && ucBits < 8)
            {
                REG_COILS_BUF[usRegIndex++] = (ucState >> ucBits) & 0X01;
                usNCoils--;
                ucBits++;
                (*pusNCoilsResponse)++;
            }
            ucLoops--;
        }
    }
    else
    {
        // 非法检测
        if(usRegIndex >= REG_COILS_SIZE)
        {
            return MB_ENOREG;
        }

		usReadableCnt = REG_COILS_SIZE - usRegIndex;  // 计算实际可读的线圈数量
		
		if(usReadableCnt < usNCoils)                  // 实际可读的线圈数量小于主机要读取的数量
		{
			usNCoils = usReadableCnt;
		}

        ucLoops = (usNCoils - 1) / 8 + 1;
        while(ucLoops != 0 && usRegIndex < REG_COILS_SIZE)
        {
            ucState = 0;
            ucBits  = 0;
            while(usNCoils != 0 && ucBits < 8 && usRegIndex < REG_COILS_SIZE)
            {
                if(REG_COILS_BUF[usRegIndex])
                {
                    ucState |= (1 << ucBits);
                }
                usNCoils--;
                usRegIndex++;
                ucBits++;
                (*pusNCoilsResponse)++;
            }
            *pucRegBuffer++ = ucState;
            ucLoops--;
        }
    }

    return MB_ENOERR;
}

/// CMD2命令处理回调函数
eMBErrorCode eMBRegDiscreteCB( UCHAR * pucRegBuffer, USHORT usAddress, USHORT usNDiscrete, USHORT* pusNDiscreteResponse)
{
    USHORT usRegIndex    = usAddress - 1;
	USHORT usReadableCnt = 0;
    UCHAR  ucBits        = 0;
    UCHAR  ucState       = 0;
    UCHAR  ucLoops       = 0;

    // 非法检测
    if(usRegIndex >= REG_DISC_SIZE)
    {
        return MB_ENOREG;
    }

    *pusNDiscreteResponse = 0;                   // 存储实际读取到的离散量输入数量

	usReadableCnt = REG_DISC_SIZE - usRegIndex;  // 计算实际存在的离散量输入寄存器数量
	
	if(usReadableCnt < usNDiscrete)              // 实际可读的离散量输入寄存器小于主机要读取的数量
	{
		usNDiscrete = usReadableCnt;
	}
	
    ucLoops = (usNDiscrete - 1) / 8 + 1;
    while(ucLoops != 0 && usRegIndex < REG_DISC_SIZE)
    {
        ucState = 0;
        ucBits  = 0;
        while(usNDiscrete != 0 && ucBits < 8 && usRegIndex < REG_DISC_SIZE)
        {
            if(REG_DISC_BUF[usRegIndex])
            {
                ucState |= (1 << ucBits);
            }
            usNDiscrete--;
            usRegIndex++;
            ucBits++;
            (*pusNDiscreteResponse)++;
        }
        *pucRegBuffer++ = ucState;
        ucLoops--;
    }

    // 模拟离散量输入被改变
    for(usRegIndex = 0; usRegIndex < REG_DISC_SIZE; usRegIndex++)
    {
        REG_DISC_BUF[usRegIndex] = !REG_DISC_BUF[usRegIndex];
    }

    return MB_ENOERR;
}
